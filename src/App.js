import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import Person from  './Person/Person';
class App extends Component {
  state ={
    persons:[
      { name:'Saad Turky', age:41 },
      { name:'Ahmed', age:59 },
      { name:'Mohammed', age:81 }
    ],
    new: 'any value'
  }

swichNameHandler = () =>{
  //console.log('Was clicked');
  // this is not allowed in javascript : this.state.persons[0].name='Saad';
  this.setState(
    {
        persons:[
          { name:'Saad Turky Al.huwaider', age:41 },
          { name:'Ahmed', age:59 },
          { name:'Mohammed', age:20 }
        ]
    }
    )
}

  render() {
    return (
      <div className="App">
       <h1>I'm Saad Turky</h1>
       <p>This is working here</p>
       <button onClick={this.swichNameHandler}>Swich Name</button>
       <Person name={this.state.persons[0].name} age={this.state.persons[0].age}>I am a programmer</Person>
       <Person name={this.state.persons[1].name} age={this.state.persons[1].age}>I am a Soldies</Person>
       <Person name={this.state.persons[2].name} age={this.state.persons[2].age}>I am a student</Person>
      </div>
    );

    // return(
    //   React.createElement('div',{className:"App"},React.createElement('h1',null,'Hi, I\'m Saad Turky'))
    // );
  }
}

export default App;
